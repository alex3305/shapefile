package com.geodan.shapefile.models;

public enum ShapeType {

    NULL(0),
    Point(1),
    Polyline(3),
    Polygon(5),
    Multipoint(8),
    PointZ(11),
    PolylineZ(13),
    PolygonZ(15),
    MultipointZ(18),
    PointM(21),
    PolylineM(23),
    PolygonM(25),
    MultipointM(28),
    Multipatch(31);

    private final int type;

    ShapeType(int type) {
        this.type = type;
    }

    public static ShapeType forCode(int code) {
        for (ShapeType t : ShapeType.values()) {
            if (t.type == code) {
                return t;
            }
        }

        return NULL;
    }

}
