package com.geodan.shapefile.models;

import com.vividsolutions.jts.geom.Coordinate;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Optional;

public class ShapefileHeader {

    private static final int FILE_CODE = 0x0000270a;

    private int fileLength;

    private int version;

    private int shapeType;

    private Coordinate boundingX;

    private Coordinate boundingY;

    private Coordinate boundingZ;

    private Coordinate boundingM;

    public static Optional<ShapefileHeader> read(Path file) {
        if (file == null || !Files.exists(file) || Files.isDirectory(file) || !Files.isReadable(file)) {
            return Optional.empty();
        }

        try (FileChannel channel = FileChannel.open(file, StandardOpenOption.READ)) {
            final ShapefileHeader header = new ShapefileHeader();
            final ByteBuffer buffer = ByteBuffer.allocate(100);
            
            channel.read(buffer);
            buffer.order(ByteOrder.BIG_ENDIAN);
            if (buffer.getInt(0) != FILE_CODE) {
                throw new IOException("File is an invalid Shapefile");
            }

            header.fileLength = buffer.getInt(24);

            buffer.order(ByteOrder.LITTLE_ENDIAN);
            header.version = buffer.getInt(28);
            header.shapeType = buffer.getInt(32);

            header.boundingX = new Coordinate(buffer.getDouble(36), buffer.getDouble(52));
            header.boundingY = new Coordinate(buffer.getDouble(44), buffer.getDouble(44));
            header.boundingZ = new Coordinate(buffer.getDouble(68), buffer.getDouble(76));
            header.boundingM = new Coordinate(buffer.getDouble(84), buffer.getDouble(92));

            return Optional.of(header);
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShapefileHeader header = (ShapefileHeader) o;
        return version == header.version &&
                shapeType == header.shapeType &&
                Objects.equals(boundingX, header.boundingX) &&
                Objects.equals(boundingY, header.boundingY) &&
                Objects.equals(boundingZ, header.boundingZ) &&
                Objects.equals(boundingM, header.boundingM);
    }

    public int getFileLength() {
        return fileLength;
    }

    public int getVersion() {
        return version;
    }

    public ShapeType getShapeType() {
        return ShapeType.forCode(this.shapeType);
    }

    public Coordinate getBoundingX() {
        return boundingX;
    }

    public Coordinate getBoundingY() {
        return boundingY;
    }

    public Coordinate getBoundingZ() {
        return boundingZ;
    }

    public Coordinate getBoundingM() {
        return boundingM;
    }
}
