package com.geodan.shapefile.models;

public class ShapefileIndex {

    private final int offset;

    private final int length;

    public ShapefileIndex(int offset, int length) {
        this.offset = offset;
        this.length = length;
    }

    public int getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }
}
