package com.geodan.shapefile.models;

public class ShapefileRecordAttribute<T> {

    private String key;

    private Class<?> type;

    private T value;

    public ShapefileRecordAttribute() {
    }

    public ShapefileRecordAttribute(String key, Class<?> type, T value) {
        this.key = key;
        this.type = type;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
