package com.geodan.shapefile.models;

import com.vividsolutions.jts.geom.Geometry;

import java.util.ArrayList;
import java.util.List;

public class ShapefileRecord {

    private final List<ShapefileRecordAttribute<?>> attributes;

    private final int length;

    private final int offset;

    private Geometry geometry;

    private ShapeType geometryType;

    private int recordNumber;

    public ShapefileRecord(int offset, int length) {
        this.attributes = new ArrayList<>();
        this.offset = offset;
        this.length = length;
    }

    public boolean addAttribute(ShapefileRecordAttribute<?> featureRecordAttribute) {
        return attributes.add(featureRecordAttribute);
    }

    public boolean contains(ShapefileRecordAttribute<?> o) {
        return attributes.contains(o);
    }

    public List<ShapefileRecordAttribute<?>> getAttributes() {
        return attributes;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public ShapeType getGeometryType() {
        return geometryType;
    }

    public int getLength() {
        return length;
    }

    public int getOffset() {
        return offset;
    }

    public int getRecordNumber() {
        return recordNumber;
    }

    public boolean hasGeometry() {
        return this.geometry != null;
    }

    public boolean removeAttribute(ShapefileRecordAttribute<?> o) {
        return attributes.remove(o);
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public void setGeometryType(int shapeType) {
        this.geometryType = ShapeType.forCode(shapeType);
    }

    public void setGeometryType(ShapeType geometryType) {
        this.geometryType = geometryType;
    }

    public void setRecordNumber(int recordNumber) {
        this.recordNumber = recordNumber;
    }
}
