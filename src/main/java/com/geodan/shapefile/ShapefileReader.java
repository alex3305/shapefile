package com.geodan.shapefile;

import com.geodan.shapefile.internal.DbfReader;
import com.geodan.shapefile.internal.ShpReader;
import com.geodan.shapefile.internal.ShxReader;
import com.geodan.shapefile.models.ShapefileHeader;
import com.geodan.shapefile.models.ShapefileRecord;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ShapefileReader {

    private final DbfReader dbfReader;

    private final ShpReader shpReader;

    private final ShxReader shxReader;

    private Path dbfFile;

    private ShapefileHeader header;

    private List<ShapefileRecord> index;

    private Path shpFile;

    private Path shxFile;

    public ShapefileReader(Path shpFile) throws IOException {
        this.validateShape(shpFile);

        this.dbfReader = new DbfReader(this.dbfFile);
        this.shpReader = new ShpReader(this.shpFile);
        this.shxReader = new ShxReader(this.shxFile);

        this.openIndex();
        this.shpReader.setShapeType(this.getHeader().getShapeType());
    }

    public ShapefileRecord findByRecord(ShapefileRecord record) throws IllegalArgumentException, IOException {
        if (record == null) {
            throw new IllegalArgumentException("Record may not be null");
        }

        return this.shpReader.findByRecord(record);
    }

    public List<ShapefileRecord> findByRecords() throws IOException {
        return this.findByRecords(this.index);
    }

    public List<ShapefileRecord> findByRecords(List<ShapefileRecord> records) throws IllegalArgumentException, IOException {
        if (records == null || records.isEmpty()) {
            throw new IllegalArgumentException("Records may not be null or empty");
        }

        return this.shpReader.findByRecords(records);
    }

    public ShapefileHeader getHeader() {
        return header;
    }

    public List<ShapefileRecord> getIndex() {
        return index;
    }

    private void openIndex() throws IOException {
        this.index = this.shxReader.open();
    }

    private ShapefileHeader readHeader(Path file) throws IOException {
        return ShapefileHeader.read(file).orElseThrow(IOException::new);
    }

    private Path replaceExtension(Path p, String newExtension) {
        String str = p.toString();
        return Paths.get(str.substring(0, str.length() - 3) + newExtension);
    }

    private void validateShape(Path shpFile) throws IOException {
        if (!shpFile.toString().endsWith(".shp")) {
            throw new IOException("Input file must be of type shp");
        }

        if (!Files.exists(shpFile)) {
            throw new IOException("Missing mandatory shp file");
        }

        final Path dbfFile = this.replaceExtension(shpFile, "dbf");
        if (!Files.exists(dbfFile)) {
            throw new IOException("Missing mandatory dbf file");
        }

        final Path shxFile = this.replaceExtension(shpFile, "shx");
        if (!Files.exists(shxFile)) {
            throw new IOException("Missing mandatory shx file");
        }

        this.header = this.readHeader(shpFile);
        if (!this.header.equals(this.readHeader(shxFile))) {
            throw new IOException("Shapefile and Shape index are different");
        }

        this.dbfFile = dbfFile;
        this.shpFile = shpFile;
        this.shxFile = shxFile;
    }

}
