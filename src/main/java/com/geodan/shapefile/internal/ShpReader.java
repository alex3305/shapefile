package com.geodan.shapefile.internal;

import com.geodan.shapefile.models.ShapeType;
import com.geodan.shapefile.models.ShapefileRecord;
import com.vividsolutions.jts.geom.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class ShpReader {

    private static final int OFFSET = 50;

    private static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();

    private final Path shpFile;

    private ShapeType shapeType;

    public ShpReader(Path shpFile) {
        this.shpFile = shpFile;
    }

    public ShapefileRecord findByRecord(ShapefileRecord record) throws IOException {
        try (FileChannel channel = FileChannel.open(this.shpFile, StandardOpenOption.READ)) {
            return this.readRecord(channel, record);
        }
    }

    public List<ShapefileRecord> findByRecords(List<ShapefileRecord> records) throws IOException {
        try (FileChannel channel = FileChannel.open(this.shpFile, StandardOpenOption.READ)) {
            for (ShapefileRecord record : records) {
                this.readRecord(channel, record);
            }

            return records;
        }
    }

    private ShapefileRecord readRecord(FileChannel channel, ShapefileRecord record) throws IOException {
        // 16-bit words are * 2 in real bytes.
        channel.position(record.getOffset() * 2);

        final ByteBuffer buffer = ByteBuffer.allocateDirect(record.getLength() * 2 + 8);
        channel.read(buffer);
        buffer.flip();

        this.readRecordHeader(buffer, record);
        record.setGeometry(this.createGeometry(record.getGeometryType(), buffer));

        return record;
    }

    private void readRecordHeader(ByteBuffer buffer, ShapefileRecord record) {
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.position(0);
        record.setRecordNumber(buffer.getInt());

        buffer.position(buffer.position() + 4);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        record.setGeometryType(buffer.getInt());
    }

    private Geometry createGeometry(ShapeType type, ByteBuffer buffer) {
        switch (type) {
            case Point:
                return this.createPoint(buffer, 2);
            case PointM:
                return this.createPoint(buffer, 3);
            case PointZ:
                return this.createPoint(buffer, 4);
            case Polyline:
                return this.createMultiLineString(buffer, 2);
            case PolylineM:
                return this.createMultiLineString(buffer, 3);
            case PolylineZ:
                return this.createMultiLineString(buffer, 4);
            case Polygon:
                return this.createPolygon(buffer, 2);
            case PolygonM:
                return this.createPolygon(buffer, 3);
            case PolygonZ:
                return this.createPolygon(buffer, 4);
            case Multipoint:
                return this.createMultiPoint(buffer, 2);
            case MultipointM:
                return this.createMultiPoint(buffer, 3);
            case MultipointZ:
                return this.createMultiPoint(buffer, 4);
            case Multipatch:
                throw new UnsupportedOperationException();
            case NULL:
                if (this.shapeType != ShapeType.NULL) {
                    return this.createGeometry(shapeType, buffer);
                }
            default:
                return null;
        }
    }

    private Coordinate createCoordinate(ByteBuffer buffer, int ordinates) {
        if (ordinates == 2) {
            return new Coordinate(buffer.getDouble(), buffer.getDouble());
        } else if (ordinates == 3) {
            return new Coordinate(buffer.getDouble(), buffer.getDouble(), buffer.getDouble());
        } else if (ordinates == 4) {
            Coordinate c = new Coordinate(buffer.getDouble(), buffer.getDouble(), buffer.getDouble());
            buffer.position(buffer.position() + 8); // 4 ordinates are unsupported in JTS, skipping it...
            return c;
        }

        return null;
    }

    private Point createPoint(ByteBuffer buffer, int ordinates) {
        return GEOMETRY_FACTORY.createPoint(this.createCoordinate(buffer, ordinates));
    }

    private MultiPoint createMultiPoint(ByteBuffer buffer, int ordinates) {
        buffer.position(buffer.position() + 32); // 36 - shapeType (4)
        int numPoints = buffer.getInt();

        Coordinate[] points = new Coordinate[numPoints];
        for (int i = 0; i < numPoints; i++) {
            points[i] = this.createCoordinate(buffer, ordinates);
        }

        return GEOMETRY_FACTORY.createMultiPoint(points);
    }

    private MultiLineString createMultiLineString(ByteBuffer buffer, int ordinates) {
        buffer.position(buffer.position() + 32); // 36 - shapeType (4)
        int numParts = buffer.getInt();
        int numPoints = buffer.getInt();
        int parts = buffer.getInt();

//        for (int i = 0; i < numParts; i++) {
//            Coordinate[] points = new Coordinate[numPoints];
//
//            for (int j = 0; i < numPoints; j++) {
//
//            }
//        }

        return null;
    }

    private Polygon createPolygon(ByteBuffer buffer, int ordinates) {
        if (ordinates == 2) {

        } else if (ordinates == 3) {

        } else {
            return null;
        }

        return null;
    }


    public void setShapeType(ShapeType shapeType) {
        this.shapeType = shapeType;
    }
}
