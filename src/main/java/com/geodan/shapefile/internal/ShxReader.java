package com.geodan.shapefile.internal;

import com.geodan.shapefile.models.ShapefileIndex;
import com.geodan.shapefile.models.ShapefileRecord;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ShxReader {

    private static final int BUFFER_SIZE = 4096;

    private final Path shxFile;

    public ShxReader(Path shxFile) {
        this.shxFile = shxFile;
    }

    public List<ShapefileRecord> open() throws IOException {
        final List<ShapefileRecord> index = new ArrayList<>();

        try (FileChannel channel = FileChannel.open(this.shxFile, StandardOpenOption.READ)) {
            channel.position(100); // Skip the header.

            final ByteBuffer buffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
            buffer.order(ByteOrder.BIG_ENDIAN);

            while (channel.read(buffer) > 0) {
                buffer.flip();

                while (buffer.position() + 4 < buffer.limit()) {
                    index.add(new ShapefileRecord(buffer.getInt(), buffer.getInt()));
                }

                buffer.clear();
            }
        }

        return index;
    }

}
