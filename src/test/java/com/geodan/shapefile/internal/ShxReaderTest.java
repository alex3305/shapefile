package com.geodan.shapefile.internal;

import com.geodan.shapefile.models.ShapefileRecord;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

/**
 * Created by alex on 5-5-2016.
 */
public class ShxReaderTest {

    private Path shxFile;

    @Before
    public void setUp() {
        this.shxFile = Paths.get("src/test/resources", "data/natural-earth/ne_10m_airports.shp");
        Assert.assertTrue(Files.exists(shxFile));
    }

    @Test
    public void read() throws Exception {
        ShxReader reader = new ShxReader(this.shxFile);
        Collection<ShapefileRecord> index = reader.open();
        Assert.assertNotNull(index);
        Assert.assertTrue(index.size() > 0);
    }

}