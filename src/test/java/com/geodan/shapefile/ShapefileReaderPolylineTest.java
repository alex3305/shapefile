package com.geodan.shapefile;

import org.junit.Assert;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by alex on 8-5-2016.
 */
public class ShapefileReaderPolylineTest extends AbstractShapefileReaderTest {
    @Override
    public void setup() {
        this.shapefile = Paths.get("src/test/resources/data/sample", "UTM_1km_31n_U.shp");
        Assert.assertTrue(Files.exists(this.shapefile));
    }
}
