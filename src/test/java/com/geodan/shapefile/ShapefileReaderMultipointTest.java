package com.geodan.shapefile;

import org.junit.Assert;

import java.nio.file.Files;
import java.nio.file.Paths;

public class ShapefileReaderMultipointTest extends AbstractShapefileReaderTest {

    @Override
    public void setup() {
        this.shapefile = Paths.get("src/test/resources/data/sample", "multipoint.shp");
        Assert.assertTrue(Files.exists(this.shapefile));
    }

}
