package com.geodan.shapefile.models;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * Created by alex on 5-5-2016.
 */
public class ShapefileHeaderTest {

    @Test
    public void read() throws Exception {
        Path shx = Paths.get("src/test/resources", "data/natural-earth/ne_10m_airports.shp");
        Assert.assertTrue(Files.exists(shx));

        Optional<ShapefileHeader> header = ShapefileHeader.read(shx);
        Assert.assertTrue(header.isPresent());
    }

}