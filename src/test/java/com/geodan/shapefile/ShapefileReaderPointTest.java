package com.geodan.shapefile;

import org.junit.Assert;

import java.nio.file.Files;
import java.nio.file.Paths;

public class ShapefileReaderPointTest extends AbstractShapefileReaderTest {

    @Override
    public void setup() {
        this.shapefile = Paths.get("src/test/resources/data/natural-earth", "ne_10m_airports.shp");
        Assert.assertTrue(Files.exists(this.shapefile));
    }

}
