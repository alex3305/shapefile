package com.geodan.shapefile;

import com.geodan.shapefile.models.ShapefileRecord;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;

public abstract class AbstractShapefileReaderTest {

    protected Path shapefile;

    @Before
    public abstract void setup();

    @Test
    public void testFindByRecord() throws Exception {
        ShapefileReader reader = new ShapefileReader(this.shapefile);
        ShapefileRecord record = reader.findByRecord(reader.getIndex().get(0));
        this.assertShapefileRecord(record);
    }

    @Test
    public void testFindByRecords() throws Exception {
        ShapefileReader reader = new ShapefileReader(this.shapefile);
        List<ShapefileRecord> records = reader.findByRecords();

        Assert.assertNotNull(records);
        Assert.assertEquals(records.size(), reader.getIndex().size());
        records.stream().forEach(this::assertShapefileRecord);
    }

    @Test
    public void testFindByRecordsSubset() throws Exception {
        ShapefileReader reader = new ShapefileReader(this.shapefile);
        List<ShapefileRecord> query = reader.getIndex().subList(0, 10);
        List<ShapefileRecord> records = reader.findByRecords(query);

        Assert.assertNotNull(records);
        records.stream().forEach(this::assertShapefileRecord);
    }

    @Test
    public void testConstruct() throws Exception {
        ShapefileReader reader = new ShapefileReader(this.shapefile);
        Assert.assertNotNull(reader);
        Assert.assertNotNull(reader.getIndex());

        Assert.assertTrue(reader.getIndex() instanceof Collection);
        Assert.assertTrue(reader.getIndex().size() > 1);
    }

    protected void assertShapefileRecord(ShapefileRecord record) {
        Assert.assertNotNull(record);
        Assert.assertNotNull(record.getLength());
        Assert.assertNotNull(record.getOffset());
//        Assert.assertNotNull(record.getGeometry());
        Assert.assertNotNull(record.getAttributes());

        Assert.assertTrue(record.getLength() > 0);
        Assert.assertTrue(record.getOffset() > 0);
//        Assert.assertTrue(record.getAttributes().size() > 0); // FIXME
    }

}