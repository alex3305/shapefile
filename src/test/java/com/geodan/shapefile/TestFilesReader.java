package com.geodan.shapefile;

import com.geodan.shapefile.models.ShapefileRecord;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class TestFilesReader {

    private static final String DIRECTORY = "./src/test/resources/data/test-files";

    private static ShapefileReader multipatchReader;

    private static ShapefileReader multipointReader;
    private static ShapefileReader multipointMReader;
    private static ShapefileReader multipointZReader;

    private static ShapefileReader pointReader;
    private static ShapefileReader pointMReader;
    private static ShapefileReader pointZReader;

    private static ShapefileReader polygonReader;
    private static ShapefileReader polygonMReader;
    private static ShapefileReader polygonZReader;

    private static ShapefileReader polylineReader;
    private static ShapefileReader polylineMReader;
    private static ShapefileReader polylineZReader;

    @BeforeClass
    public static void setUp() throws IOException {
        multipatchReader = new ShapefileReader(Paths.get(DIRECTORY, "multipatch.shp"));

        multipointReader = new ShapefileReader(Paths.get(DIRECTORY, "multipoint.shp"));
        multipointMReader = new ShapefileReader(Paths.get(DIRECTORY, "multipointm.shp"));
        multipointZReader = new ShapefileReader(Paths.get(DIRECTORY, "multipointz.shp"));

        pointReader = new ShapefileReader(Paths.get(DIRECTORY, "point.shp"));
        pointMReader = new ShapefileReader(Paths.get(DIRECTORY, "pointm.shp"));
        pointZReader = new ShapefileReader(Paths.get(DIRECTORY, "pointz.shp"));

        polygonReader = new ShapefileReader(Paths.get(DIRECTORY, "polygon.shp"));
        polygonMReader = new ShapefileReader(Paths.get(DIRECTORY, "polygonm.shp"));
        polygonZReader = new ShapefileReader(Paths.get(DIRECTORY, "polygonz.shp"));

        polylineReader = new ShapefileReader(Paths.get(DIRECTORY, "polyline.shp"));
        polylineMReader = new ShapefileReader(Paths.get(DIRECTORY, "polylinem.shp"));
        polylineZReader = new ShapefileReader(Paths.get(DIRECTORY, "polylinez.shp"));
    }

    @Test
    @Ignore
    public void testMultipatch() throws IOException {
        Assert.fail("Unsupported");
    }

    @Test
    public void testMultipoint() throws IOException {
        List<ShapefileRecord> records = multipointReader.findByRecords();
        this.assertRecords(records);
    }

    @Test
    public void testMultipointM() throws IOException {
        List<ShapefileRecord> records = multipointMReader.findByRecords();
        this.assertRecords(records);
    }

    @Test
    public void testMultipointZ() throws IOException {
        List<ShapefileRecord> records = multipointZReader.findByRecords();
        this.assertRecords(records);
    }

    @Test
    public void testPoint() throws IOException {
        List<ShapefileRecord> records = pointReader.findByRecords();
        this.assertRecords(records);
    }

    @Test
    public void testPointM() throws IOException {
        List<ShapefileRecord> records = pointMReader.findByRecords();
        this.assertRecords(records);
    }

    @Test
    public void testPointZ() throws IOException {
        List<ShapefileRecord> records = pointZReader.findByRecords();
        this.assertRecords(records);
    }

    @Test
    @Ignore
    public void testPolygon() throws IOException {
        Assert.fail("Unsupported");
    }

    @Test
    @Ignore
    public void testPolygonM() throws IOException {
        Assert.fail("Unsupported");
    }

    @Test
    @Ignore
    public void testPolygonZ() throws IOException {
        Assert.fail("Unsupported");
    }

    @Test
    @Ignore
    public void testPolyline() throws IOException {
        Assert.fail("Unsupported");
    }

    @Test
    @Ignore
    public void testPolylineM() throws IOException {
        Assert.fail("Unsupported");
    }

    @Test
    @Ignore
    public void testPolylineZ() throws IOException {
        Assert.fail("Unsupported");
    }

    private void assertRecords(List<ShapefileRecord> records) {
        Assert.assertNotNull(records);
        Assert.assertTrue(records.size() > 0);
        records.stream().forEach(this::assertRecord);
    }

    private void assertRecord(ShapefileRecord record) {
        Assert.assertNotNull(record);
        Assert.assertNotNull(record.getLength());
        Assert.assertNotNull(record.getOffset());
        Assert.assertNotNull(record.getGeometry());
        Assert.assertNotNull(record.getAttributes());

        Assert.assertTrue(record.getLength() > 0);
        Assert.assertTrue(record.getOffset() > 0);
    }

}
